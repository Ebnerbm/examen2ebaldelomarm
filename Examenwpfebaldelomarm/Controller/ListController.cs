﻿using Examenwpfebaldelomarm.Model;
using Examenwpfebaldelomarm.View;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Examenwpfebaldelomarm.Controller
{
    public class ListController
    {
        object pwindow;
        SaveFileDialog sfdialog;
        OpenFileDialog ofdialog;
        public ListController(ListWindow window)
        {
            pwindow = window;
            sfdialog = new SaveFileDialog();
            ofdialog = new OpenFileDialog();
        }


        public void BookEventHandler(object sender, RoutedEventArgs e)
        {
            Button B = (Button)sender;
            switch (B.Name)
            {
                case "SaveButton":
                    SaveData();
                    break;
                case "OpenButton":
                    OpenFile();
                    break;
                case "ClearButton":
                    ((ListWindow)pwindow).Clear();
                    break;
            }
        }

        private void OpenFile()
        {
            ofdialog.Filter = "Xml File (*.Xml)|*.Xml";
            if (ofdialog.ShowDialog() == true)
            {
                Listm p = new Listm();
                if (pwindow.GetType().Equals(typeof(ListWindow)))
                {
                    ((ListWindow)pwindow).SetData(p.FromXml(ofdialog.FileName));
                }

            }


        }


        private void SaveData()
        {
            sfdialog.Filter = "Xml File (*.Xml)|*.Xml";
            if (sfdialog.ShowDialog() == true)
            {
                Listm p;
                if (pwindow.GetType().Equals(typeof(ListWindow)))
                {
                    p = ((ListWindow)pwindow).GetData();
                    p.ToXml(sfdialog.FileName);
                }



            }
        }
        

    }
}
