﻿using Examenwpfebaldelomarm.Interfaces;
using Examenwpfebaldelomarm.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examenwpfebaldelomarm.Model
{
    [Serializable]
    public class Listm : IToFile, IFromFile<Listm>
    {
       
        public string Name { get; set; }
        



        public Listm FromXml(string filepath)
        {
            return XmlSerialization.ReadFromXmlFile<Listm>(filepath);
        }

        public void ToXml(string filepath)
        {
            XmlSerialization.WriteToXmlFile(filepath, this);
        }
    }
}
