﻿using Examenwpfebaldelomarm.Controller;
using Examenwpfebaldelomarm.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


namespace Examenwpfebaldelomarm.View
{
    /// <summary>
    /// Interaction logic for ListWindow.xaml
    /// </summary>
    public partial class ListWindow : Window
    {
        ListController lc;
        public ListWindow()
        {
            InitializeComponent();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            SetupController();
        }
        public Listm GetData()
        {
            Listm book = new Listm();
            book.Name = NameTextBox.Text;
           
            return book;
        }

        public void SetData(Listm list)
        {
            NameTextBox.Text = list.Name;
            
        }

        public void Clear()
        {
            NameTextBox.Text = "";
        }


        protected void SetupController()
        {
            lc = new ListController(this);
            this.SaveButton.Click += new RoutedEventHandler(lc.BookEventHandler);
            this.OpenButton.Click += new RoutedEventHandler(lc.BookEventHandler);
            this.ClearButton.Click += new RoutedEventHandler(lc.BookEventHandler);
        }
    }
}
